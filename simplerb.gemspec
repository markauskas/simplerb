# coding: utf-8
lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)
require 'simplerb/version'

Gem::Specification.new do |spec|
  spec.name        = 'simplerb'
  spec.version     = Simplerb::VERSION
  spec.licenses    = ['MIT']
  spec.authors     = ["Luca Ongaro"]
  spec.email       = ["lukeongaro@gmail.com"]
  spec.summary     = %q{A super simple dynamic website framework for educational purpose}
  spec.description = %q{Simplerb is a super simple and minimal framework for generating dynamic websites "PHP-style" but with Ruby and ERB. Its main purpose is to help teaching web development to beginners.}

  spec.required_ruby_version = '>= 1.9.2'

  spec.files = Dir["lib/**/*", "README.md"]

  spec.executables   = ['simplerb']
  spec.require_paths = ['lib']

  spec.add_dependency 'sinatra'
  spec.add_dependency 'thor'

  spec.add_development_dependency 'rake'
end
