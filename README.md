# Simplerb

A super simple framework for creating dynamic websites with Ruby and ERB.
Basically PHP-style web development in Ruby. The main purpose is to teach web
development to beginners.


# Installation

Just install the gem by running `gem install simplerb`


# Getting Started

To create a new project called `hello_world`, run this from the console:

```
simplerb new hello_world
```

This will create a directory called `hello_world` with some files and
subdirectories in it:

```
 - hello_world
   \_ views
   |_ public
   |_ config.ru
```

Now you can start the server by running `simplerb server`. Open your browser and
visit `http://localhost:9292`: if everything is up and running you should now
see a page with some greetings.


# Adding pages

You can add pages by creating `erb` files in the `views` directory. For example,
try creating a file `views/hello.erb` containing the text "Hello, World!". Now,
if you visit `http://localhost:9292/hello`, you should see that text.

If you want to reuse a part of a page (a "partial") in multiple pages, just
extract it into a separate file with a name starting with an underscore (e.g.
`_my_shared_content`). Then, within a page, call `<%= partial 'my_shared_content' %>`
to render the partial.


# Static files

Any static file that you put in the `public` folder will be served at the
corresponding URL, so if you create a file `public/styles/my_style.css`,
it will be served at `http://localhost:9292/styles/my_style.css`.
