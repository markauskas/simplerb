require 'thor'
require 'thor/actions'
require 'pathname'

module Simplerb
  class CLI < Thor
    include Thor::Actions

    desc 'new NAME', 'create a new Simplerb project called NAME'
    def new(name)
      target = Pathname.pwd.join(name)
      self.destination_root = target
      options = { name: name }
      directory 'views', options
      directory 'public', options
      template 'Gemfile.tt', options
      template 'config.ru.tt', options
      inside target do
        run 'bundle install'
      end
    end

    desc 'server', 'start a local server'
    option :port, desc: 'specify the port', type: :numeric, default: 9292, aliases: [:p]
    option :server, desc: 'specify the webserver', type: :string, default: 'webrick', aliases: [:s]
    def server
      exec "rackup -p #{options[:port]} -s #{options[:server]}"
    end

    def self.source_root
      File.expand_path(File.join(File.dirname(__FILE__), 'templates'))
    end
  end
end
